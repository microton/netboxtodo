Pro uživatele:
------------------------------------------------------------------------------------
Struktura uživatele:
{
    id: number,
    name: string
}

GET users
- vrátí všechny uživatele

GET users/:userId
- vrátí uživatele s userId

POST users
- payload:
{
    name: 'User name'
}
- vytvoří nového uživatele
- vygeneruje unikátní ID uživatele (ideálně číselné 1,2,3...)

PUT users/:userId
- payload:
{
    name: 'User name'
}
- aktualizuje existujícího uživatele

DELETE users/:userId
- vymaže uživatele s userId

#####################################################################################

Pro úkoly:
------------------------------------------------------------------------------------
Struktura úkolu:
{
    id: number,
    name: string,
    description: string,
    deadline: Date,     // ve formátu yyyy-mm-dd
    user: {
        id:  number,
        name: string
    }
}

GET tasks/(?user=userId&expand=user&limited_deadline=true|false)
- vrátí všechny úkoly
- parametry:
    user - úkoly pro daného uživatele
    expand - při jediné povolené hodnotě 'user' budou v odpovědi 
             obsaženy detaily každého uživatele, jinak pouze jejich ID
    limited_deadline - úkoly s limitem vypracování do jednoho týdne

GET tasks/:taskId
- vrátí úkol s taskId

POST tasks
- payload:
{
    name: string,
    description: string,
    deadline: Date,
    user: userId
}
- vytvoří nový úkol
- vygeneruje unikátní ID úkolu (ideálně číselné 1,2,3...)

PUT tasks/:taskId
- payload:
{
    name: string,
    description: string,
    deadline: Date,
    user: userId
}
- aktualizuje existující úkol

DELETE tasks/:taskId
- vymaže úkol s taskId

# NetboxTODO

Projekt je NetboxTODO je složen ze dvou částí: Client a Server

## Client

Angular 4 (verze 4.4.4)

## Server

Django REST API

## Demo

Ukázku projektu lze najít zde: http://netboxtodo.microton.cz

## Deploy

Projekt lze spustit na platformě Docker (ukázka projektu je spuštěna právě na této technologii)
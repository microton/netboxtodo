import { AbstractControl, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

export class ValidationService {

    constructor() { }

    invalidateForm(control: AbstractControl) {
        if (control.hasOwnProperty('controls')) {
            control.markAsDirty(); // mark group
            const ctrl = <FormGroup>control;
            for (const inner in ctrl.controls) {
                if (ctrl.controls.hasOwnProperty(inner)) {
                    this.invalidateForm(ctrl.controls[inner]);
                }
            }
        } else {
            control.markAsDirty();
        }
    }

}

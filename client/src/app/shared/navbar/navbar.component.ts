import { NavigationStart, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import 'rxjs/add/operator/filter';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    isCollapsed = true;

    constructor(private router: Router) { }

    ngOnInit() {
        this.router.events
            .filter(event => event instanceof NavigationStart)
            .subscribe(event => {
                this.isCollapsed = true;
            });
    }

}

import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiService {

    constructor(
        private http: Http,
        private router: Router) { }

    get api() {
        return environment.api;
    }

    get server() {
        return environment.server;
    }

    get(action, params = null) {
        return this.http
            .get(`${environment.api}/${action}`, params)
            .map(this.extractData)
            .catch(error => this.handleError(error));
    }

    post(action, body) {
        return this.http
            .post(`${environment.api}/${action}`, body)
            .map(this.extractData)
            .catch(error => this.handleError(error));
    }

    put(action, body) {
        return this.http
            .put(`${environment.api}/${action}`, body)
            .map(this.extractData)
            .catch(error => this.handleError(error));
    }

    delete(action) {
        return this.http
            .delete(`${environment.api}/${action}`)
            .map(this.extractData)
            .catch(error => this.handleError(error));
    }

    private extractData(res: Response) {
        const body = res.json();
        return body || {};
    }

    private handleError(error) {
        if (error) {
            const errorData = error.json ? error.json() : error;
            return Observable.throw(errorData);
        }
    }
}

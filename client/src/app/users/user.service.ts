import { Injectable } from '@angular/core';
import { ApiService } from './../shared/api/api.service';
import { User } from './user';

@Injectable()
export class UserService {

    constructor(private apiService: ApiService) { }

    getUsers() {
        return this.apiService.get('users');
    }

    getUser(id) {
        return this.apiService.get(`users/${id}`);
    }

    addUser(user: User) {
        return this.apiService.post('users', user);
    }

    updateUser(id, user) {
        return this.apiService.put(`users/${id}`, user);
    }

    deleteUser(id) {
        return this.apiService.delete(`users/${id}`);
    }
}

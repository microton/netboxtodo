import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderModule } from 'ngx-order-pipe';
import { UsersRoutingModule } from './users-routing.module';
import { UserService } from './user.service';
import { UsersComponent } from './users.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UsersListComponent } from './users-list/users-list.component';

@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule,
        FormsModule,
        OrderModule
    ],
    declarations: [UsersComponent, UserFormComponent, UsersListComponent],
    providers: []
})
export class UsersModule { }

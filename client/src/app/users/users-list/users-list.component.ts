import { Component, OnInit } from '@angular/core';
import { User } from './../user';
import { UserService } from '../user.service';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

    users: Array<User>;
    sortField = 'id';

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.userService.getUsers().subscribe(users => {
            this.users = users;
        });
    }

    onDeleteUser(userId) {
        this.userService.deleteUser(userId).subscribe(() => {
            // reload users
            this.userService.getUsers().subscribe(users => {
                this.users = users;
            });
        });
    }

    isSortField(sortField) {
        return this.sortField === sortField;
    }

    onSetSortField(sortField) {
        this.sortField = sortField;
    }

}

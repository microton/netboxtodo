import { HttpModule } from '@angular/http';
import { ApiService } from '../../shared/api/api.service';
import { UserService } from './../user.service';
import { ValidationService } from './../../shared/validation/validation.service';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserFormComponent } from './user-form.component';

describe('UserFormComponent', () => {
    let component: UserFormComponent;
    let fixture: ComponentFixture<UserFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [UserFormComponent],
            imports: [
                RouterTestingModule,
                FormsModule,
                HttpModule
            ],
            providers: [
                ValidationService,
                UserService,
                ApiService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display new user title', () => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h4').textContent).toContain('Nový uživatel');
    });

    it('should display name of user', () => {
        component.edit = true;
        component.user = {
            id: 1,
            name: 'Pavel Novák'
        };
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h4').textContent).toContain('Pavel Novák');
    });
});


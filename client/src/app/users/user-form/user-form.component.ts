import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ValidationService } from './../../shared/validation/validation.service';
import { UserService } from '../user.service';
import { User } from './../user';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

    user: User;
    edit = false;

    @ViewChild('userForm') userForm: NgForm;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private validationService: ValidationService,
        private userService: UserService) { }

    ngOnInit() {
        this.edit = this.activatedRoute.snapshot.data['type'] === 'edit';
        if (this.edit) {
            this.userService.getUser(parseInt(this.activatedRoute.snapshot.params['id'], 10)).subscribe(user => {
                this.user = user;
            });
        } else {
            this.user = new User();
        }
    }

    onSubmit(model: User) {
        if (this.userForm.invalid) {
            return this.validationService.invalidateForm(this.userForm.control);
        }
        this.user.name = model.name;
        const observable = this.edit ? this.userService.updateUser(this.user.id, this.user) : this.userService.addUser(this.user);
        observable.subscribe(() => {
            this.router.navigate(['/users']);
        });
    }
}

import { ValidationService } from './shared/validation/validation.service';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from './users/user.service';
import { ApiService } from './shared/api/api.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavbarComponent } from './shared/navbar/navbar.component';

@NgModule({
    declarations: [
        AppComponent,
        NotFoundComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        NgbModule.forRoot()
    ],
    providers: [
        ApiService,
        ValidationService,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

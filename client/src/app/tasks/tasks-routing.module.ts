import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskFormComponent } from './task-form/task-form.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksComponent } from './tasks.component';


const routes: Routes = [
    {
        path: '',
        component: TasksComponent,
        children: [
            {
                path: '',
                component: TasksListComponent,
            },
            {
                path: 'list',
                redirectTo: ''
            },
            {
                path: 'edit/:id',
                component: TaskFormComponent,
                data: {
                    type: 'edit'
                }
            },
            {
                path: 'add',
                component: TaskFormComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TasksRoutingModule { }

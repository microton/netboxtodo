import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../users/user.service';
import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { User } from './../../users/user';
import { Task } from './../task';

@Component({
    selector: 'app-tasks-list',
    templateUrl: './tasks-list.component.html',
    styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit {

    tasks: Array<Task>;
    allTasks: Array<Task>;
    users: Array<User>;
    sortField = 'deadline';
    filterParams = {};

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private taskService: TaskService,
        private userService: UserService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.filterParams['user'] = parseInt(params['user'], 10) || undefined;
            this.filterParams['limited_deadline'] = params['limited_deadline'];
            this.getTasks();
        });
    }

    getTasks() {
        this.taskService.getTasks(this.filterParams).subscribe(tasks => {
            this.tasks = tasks.sort();
            // store all tasks for filter uses
            this.allTasks = this.tasks.slice();
            this.userService.getUsers().subscribe(users => {
                this.users = users;
            });
        });
    }

    onDeleteTask(taskId) {
        this.taskService.deleteTask(taskId).subscribe(() => {
            this.getTasks();
        });
    }

    onSelectedUserChanged(user) {
        this.filterParams['user'] = user;
        this.generateFilter();
    }

    onLimitedDeadlineChanged(limitedDeadline) {
        this.filterParams['limited_deadline'] = limitedDeadline;
        this.generateFilter();
    }

    generateFilter() {
        const filterNav = {};
        if (this.filterParams['user']) {
            filterNav['user'] = this.filterParams['user'];
        }
        if (this.filterParams['limited_deadline']) {
            filterNav['limited_deadline'] = this.filterParams['limited_deadline'];
        }
        this.router.navigate([filterNav]);
    }

    isSortField(sortField) {
        return this.sortField === sortField;
    }

    onSetSortField(sortField) {
        this.sortField = sortField;
    }
}

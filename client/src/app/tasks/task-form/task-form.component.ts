import { ValidationService } from '../../shared/validation/validation.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../users/user.service';
import { User } from './../../users/user';
import { TaskService } from './../task.service';
import { Task } from '../task';

@Component({
    selector: 'app-task-form',
    templateUrl: './task-form.component.html',
    styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {

    users: Array<User>;
    taskForm: FormGroup;
    edit = false;
    task: Task;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private validationService: ValidationService,
        private taskService: TaskService,
        private userService: UserService) { }

    ngOnInit() {
        this.createTaskForm();
        this.userService.getUsers().subscribe(users => {
            this.users = users;
            this.edit = this.activatedRoute.snapshot.data['type'] === 'edit';
            if (this.edit) {
                this.taskService.getTask(parseInt(this.activatedRoute.snapshot.params['id'], 10)).subscribe(task => {
                    this.task = task;
                    if (!this.task) {
                        alert('Tento úkol neexistuje');
                        return this.router.navigate(['/tasks']);
                    }
                    this.initTaskForm();
                });
            } else {
                this.checkParams();
            }
        });
    }

    createTaskForm() {
        this.taskForm = this.formBuilder.group({
            id: [''],
            name: ['', Validators.required],
            description: ['', Validators.required],
            deadline: ['', Validators.required],
            user: [null, Validators.required]
        });
    }

    initTaskForm() {
        this.taskForm.setValue({
            id: this.task.id,
            name: this.task.name,
            description: this.task.description,
            deadline: {
                year: this.task.deadline.getFullYear(),
                month: this.task.deadline.getMonth() + 1,
                day: this.task.deadline.getDate()
            },
            user: this.task.user
        });
    }

    checkParams() {
        const userId = parseInt(this.activatedRoute.snapshot.params['user'], 10);
        if (this.users.find(user => user.id === userId)) {
            this.taskForm.get('user').setValue(userId);
        }
    }

    onSubmit() {
        if (this.taskForm.invalid) {
            return this.validationService.invalidateForm(this.taskForm);
        }
        const model = this.taskForm.value;
        if (model.deadline) {
            model.deadline = `${model.deadline.year}-${model.deadline.month}-${model.deadline.day}`;
        }
        if (this.edit) {
            this.taskService.updateTask(this.task.id, model).subscribe(() => {
                this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
            });
        } else {
            this.taskService.addTask(model).subscribe(() => {
                this.router.navigate(['../'], { relativeTo: this.activatedRoute });
            });
        }
    }
}

import { User } from './../users/user';
export interface Task {
    id: number;
    name: String;
    description: String;
    deadline: Date;
    user: User;
}

import { UserService } from './../users/user.service';
import { Injectable } from '@angular/core';
import { ApiService } from './../shared/api/api.service';
import { Task } from './task';

import 'rxjs/add/operator/map';

@Injectable()
export class TaskService {


    constructor(
        private apiService: ApiService,
        private userService: UserService) { }

    getTasks(filter = null) {
        return this.apiService.get(`tasks`, { params: { expand: 'user', ...filter}})
            .map(tasks =>
                tasks.map(this.mapDeadline)
            );
    }

    getTask(id) {
        return this.apiService.get(`tasks/${id}`)
            .map(this.mapDeadline);
    }

    addTask(task: Task) {
        return this.apiService.post('tasks/', task);
    }

    updateTask(id, task) {
        return this.apiService.put(`tasks/${id}`, task);
    }

    deleteTask(id) {
        return this.apiService.delete(`tasks/${id}`);
    }

    private mapDeadline(task) {
        task.deadline = new Date(task.deadline);
        return task;
    }
}


# NetboxTODO Server

## Prerequisites

- [Python 3](https://www.python.org/downloads/)

## Development

```bash
python3 -m venv .venv/netbox_todo
source .venv/netbox_todo/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

## Run unit tests

```bash
python manage.py test -v2
```

## Production

In production `DJANGO_SETTINGS_MODULE` and `SECRET_KEY` environment variables must be set. 

Example:
```bash
DJANGO_SETTINGS_MODULE=netbox_todo.settings.prod
SECRET_KEY=some-really-secret-key
```
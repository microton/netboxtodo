
from rest_framework import status
from rest_framework.test import APITestCase


class BaseAPITestCase(APITestCase):
    def assertResponseIdsEqual(self, response, ids):
        response_ids = [item['id'] for item in response.data]
        self.assertSequenceEqual(response_ids, ids)

    def assertResponseIsSuccess(self, response):
        status_code = response.status_code
        self.assertTrue(status.is_success(response.status_code),
                        "Response status code %d not in 200-299 range."
                        % status_code)


class UsersAPITestCase(BaseAPITestCase):
    fixtures = ['api_test_data.json']
    users_url = '/api/users'

    def test_list_users(self):
        response = self.client.get(self.users_url)
        self.assertResponseIsSuccess(response)
        self.assertEqual(len(response.data), 2)

    def test_create_user(self):
        response = self.client.post(self.users_url, {
            'name': 'John Doe',
        })
        self.assertResponseIsSuccess(response)
        self.assertTrue('id' in response.data)
        new_user_id = response.data['id']

        response = self.client.get(self.users_url)
        self.assertEqual(len(response.data), 3)
        self.assertIn({'id': new_user_id, 'name': 'John Doe'}, response.data)


class TasksAPITestCase(BaseAPITestCase):
    fixtures = ['api_test_data.json']
    tasks_url = '/api/tasks'
    task_url = '/api/tasks/1'

    def test_list_tasks(self):
        response = self.client.get(self.tasks_url)
        self.assertResponseIsSuccess(response)
        self.assertResponseIdsEqual(response, (3, 1, 2))

    def test_list_tasks_for_user(self):
        response = self.client.get(self.tasks_url, {'user': 1})
        self.assertResponseIsSuccess(response)
        self.assertResponseIdsEqual(response, (3, 2))

        response = self.client.get(self.tasks_url, {'user': 2})
        self.assertResponseIsSuccess(response)
        self.assertResponseIdsEqual(response, (1,))

    def test_create_task(self):
        response = self.client.post(self.tasks_url, {
            'name': 'Task1 New',
            'description': 'Desc1 New',
            'deadline': '2018-01-01',
            'user': 1,
        })
        self.assertResponseIsSuccess(response)
        self.assertTrue('id' in response.data)
        new_task_id = response.data['id']

        response = self.client.get(self.tasks_url)
        self.assertResponseIdsEqual(response, (new_task_id, 3, 1, 2))

    def test_update_task(self):
        response = self.client.put(self.task_url, {
            'name': 'Task1 New',
            'description': 'Desc1 New',
            'deadline': '2018-01-01',
            'user': 1,
        })
        self.assertResponseIsSuccess(response)

        response = self.client.get(self.tasks_url)
        self.assertResponseIdsEqual(response, (1, 3, 2))
        self.assertEqual(response.data[0]['name'], 'Task1 New')

    def test_delete_task(self):
        response = self.client.delete(self.task_url)
        self.assertResponseIsSuccess(response)

        response = self.client.get(self.tasks_url)
        self.assertResponseIdsEqual(response, (3, 2))

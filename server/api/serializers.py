
from rest_framework import serializers

from tasks.models import Task
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name')


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'name', 'description', 'deadline', 'user')


class TaskSerializerWithExpandedUser(TaskSerializer):
    user = UserSerializer()


from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^users/?$', views.UserList.as_view()),
    url(r'^users/(?P<pk>\d+)/?$', views.UserDetail.as_view()),

    url(r'^tasks/?$', views.TaskList.as_view()),
    url(r'^tasks/(?P<pk>\d+)/?$', views.TaskDetail.as_view()),
]

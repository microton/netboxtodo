
from rest_framework import generics
from rest_framework import serializers
from rest_framework import fields

from tasks.models import Task
from users.models import User

from .serializers import UserSerializer, TaskSerializer, \
    TaskSerializerWithExpandedUser


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ExpandableUserSerializerMixin(object):
    def should_expand_user(self):
        expand = self.request.query_params.get('expand')
        return self.request.method == 'GET' and expand == 'user'

    def get_serializer_class(self):
        if self.should_expand_user():
            return TaskSerializerWithExpandedUser
        return TaskSerializer


class TaskList(ExpandableUserSerializerMixin, generics.ListCreateAPIView):
    def get_queryset(self):
        serializer = self.QueryParamsSerializer(data=self.request.query_params)
        _ = serializer.is_valid(raise_exception=True)
        params = serializer.validated_data

        queryset = Task.objects.all()
        if params['user'] is not None:
            queryset = queryset.for_user(params['user'])

        if params['limited_deadline']:
            queryset = queryset.ending_in_days(7)

        if self.should_expand_user():
            # prevent multiple db lookups
            queryset = queryset.select_related('user')

        return queryset

    class QueryParamsSerializer(serializers.Serializer):
        user = fields.IntegerField(required=False, default=None)
        limited_deadline = fields.BooleanField(required=False, default=False)


class TaskDetail(ExpandableUserSerializerMixin,
                 generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()

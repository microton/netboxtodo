
from .base import *

DEBUG = True
CORS_ORIGIN_ALLOW_ALL = DEBUG

SECRET_KEY = '^4yb0%qnbfbi%0)w0$hp%&9te4t0$us%8(63^4m%(enk%lg%u0'

INSTALLED_APPS += [
    'corsheaders',
]

MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev_db.sqlite3'),
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'django.template': {
            'handlers': ['console'],
            # Prevent missing context variables logs in DEBUG
            'level': 'INFO',
            'propagate': False,
        },
    },
}


import datetime

from django.db import models

from users.models import User


class TaskQuerySet(models.QuerySet):
    def for_user(self, user):
        return self.filter(user=user)

    def ending_in_days(self, days):
        today = datetime.date.today()
        days_delta = datetime.timedelta(days=days)
        deadline = today + days_delta
        return self.filter(deadline__lte=deadline)


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='tasks')
    name = models.CharField(max_length=200)
    deadline = models.DateField(db_index=True)
    description = models.TextField()

    objects = TaskQuerySet.as_manager()

    class Meta:
        ordering = ['-deadline']

    def __str__(self):
        return self.name
